/*
 ************************************************************************************
 * Copyright (C) 2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global Backbone */

(function() {
  var DocumentNoList = OB.Data.ExtensibleModel.extend({
    modelName: 'DocumentNoList',
    tableName: 'documentnolist',
    entityName: 'DocumentNoList',
    local: true,
    source: ''
  });

  DocumentNoList.addProperties([
    {
      name: 'id',
      column: 'id',
      primaryKey: true,
      type: 'TEXT'
    },
    {
      name: 'documentNo',
      column: 'documentNo',
      type: 'TEXT'
    },
    {
      name: 'date',
      column: 'date',
      type: 'NUMERIC'
    }
  ]);

  DocumentNoList.addIndex([
    {
      name: 'documentnolist_documentNo',
      columns: [
        {
          name: 'documentNo',
          sort: 'asc'
        }
      ]
    }
  ]);

  OB.Data.Registry.registerModel(DocumentNoList);
})();
