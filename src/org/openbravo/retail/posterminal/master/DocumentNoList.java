/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.retail.posterminal.master;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.mobile.core.model.HQLPropertyList;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.mobile.core.model.ModelExtensionUtils;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.retail.posterminal.POSUtils;
import org.openbravo.retail.posterminal.ProcessHQLQuery;

public class DocumentNoList extends ProcessHQLQuery {
  public static final String documentNoListPropertyExtension = "OBPOS_DocumentNoListExtension";

  @Inject
  @Any
  @Qualifier(documentNoListPropertyExtension)
  private Instance<ModelExtension> extensions;

  @Override
  protected boolean isAdminMode() {
    return true;
  }

  @Override
  protected Map<String, Object> getParameterValues(JSONObject jsonsent) throws JSONException {
    try {
      OBContext.setAdminMode(true);
      OBPOSApplications posDetail = POSUtils.getTerminalById(RequestContext.get()
          .getSessionAttribute("POSTerminal").toString());
      Map<String, Object> paramValues = new HashMap<String, Object>();
      if (jsonsent.has("parameters") && jsonsent.getJSONObject("parameters").has("dateLimit")) {
        paramValues.put("dateLimit", jsonsent.getJSONObject("parameters")
            .getJSONObject("dateLimit").getString("value"));
      }
      paramValues.put("posId", posDetail.getId());

      return paramValues;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {

    HQLPropertyList regularDocumentNoListHQLProperties = ModelExtensionUtils
        .getPropertyExtensions(extensions);

    String hql = "select " + regularDocumentNoListHQLProperties.getHqlSelect()
        + " from Order as order where " + " order.orderDate > to_date(:dateLimit, 'DD-MM-YYYY') "
        + " and order.obposApplications.id = :posId ";

    return Arrays.asList(new String[] { hql });
  }

  @Override
  protected boolean bypassPreferenceCheck() {
    return true;
  }
}