/*
 ************************************************************************************
 * Copyright (C) 2013-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.retail.posterminal;

import org.openbravo.mobile.core.CoreAppCacheResourceProvider;

/**
 * Implement this interface to provide resources to AppCache
 * 
 * @author alostale
 * @deprecated use {@link CoreAppCacheResourceProvider}
 */
@Deprecated
public interface POSAppCacheResourceProvider extends CoreAppCacheResourceProvider {
}
